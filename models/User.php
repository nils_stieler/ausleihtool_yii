<?php

namespace app\models;

use phpDocumentor\Reflection\Types\Null_;
use PHPUnit\Framework\Error\Error;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use Yii;
use function Sodium\add;

/**
 * Class User
 * @package app\models
 *
 * @property string $username
 * @property string $password
 * @property integer $role
 */
class User extends ActiveRecord implements IdentityInterface
{
    const isAdmin = NUll;
    public $newpassword = Null;
    public $password2 = Null;

    public static function tableName()
    {
        return "user";
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['username', 'password', 'password2', 'newpassword'], 'string', 'max' => 255],
            [['role'], 'integer', 'max' => 5],
        ];
    }

    /** * {@inheritdoc} */
    public static function findIdentity($id)
    {
        try {
            return static::findOne($id);
        } catch (\yii\base\InvalidConfigException $e) {
            echo "DB Error";
        }
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return false;
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return self::findOne(['username' => $username]);
    }

    /**
     *
     * @param int $role
     * @return static|Null
     */

    public static function isAdmin($role)
    {
        if ($role == 3) {
            return 1;
        } else {
            return Null;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return false;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->getSecurity()->validatePassword($password, $this->password);
    }

    public function beforeSave($insert)
    {
        if (empty($this->newpassword) && $this->isNewRecord) {
            $this->addError("newpassword", "Bitte gib ein Passwort ein");
            return false;
        }
        if (strlen($this->newpassword) < 5) {
            $this->addError("newpassword", "Dein Passwort entspricht nicht der Mindestlänge");
            return false;
        }
        if (!empty($this->newpassword)) {
            if ($this->newpassword == $this->password2) {
                $this->password = Yii::$app->getSecurity()->generatePasswordHash($this->newpassword);
            } else {
                $this->addError("password2", "Die Passwörter stimmen nicht überein");
                return false;
            }
        }

        return parent::beforeSave($insert);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Benutzername',
            'password' => 'Passwort',
            'role' => 'Rolle',
        ];
    }
}
